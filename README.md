# text2map.corpora.repo

This is a repository of large text analysis corpora which can be downloaded directly or through `text2map.corpora` (see for corpus documentation).